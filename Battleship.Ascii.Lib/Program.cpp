#include "Program.h"

#include <iostream>
#include <stdio.h>
#include <time.h>
#include <algorithm>
#include <stdexcept>
#include <stdlib.h>

#include "../Battleship.GameController.Lib/GameController.h"

#pragma comment(lib,"winmm.lib")  //for MSV C++   

using namespace Battleship::GameController;
using namespace Battleship::GameController::Contracts;


constexpr char DASHES[] = "----------------------------------------------------\n";
constexpr char DOUBLE_DASHES[] = "----------------------------------------------------\n----------------------------------------------------\n";

int BOARD_WIDTH = 0;
int BOARD_HEIGHT = 0;

namespace Color
{
std::ostream& operator<<(std::ostream& os, Code code) {
    return os << "\033[" << static_cast<int>(code) << "m";
}
}

namespace Battleship
{
  namespace Ascii
  {
    ostream& operator<<(ostream &out, Position pos)
    {
      out << (char)('A' + pos.Column) << pos.Row;
      return out;
    }

    Program::Program()
    {
    }

    Program::~Program()
    {
    }

    list<Ship> Program::myFleet;
    list<Ship> Program::enemyFleet;
    int Program::myFleetSize;
    int Program::enemyFleetSize;
    
    void Program::Main()
    {
      cout << Color::BG_DEFAULT;
      cout << Color::FG_CYAN;
      cout << R"(                                     |__                                       )" << endl;
      cout << R"(                                     | \ /                                     )" << endl;
      cout << R"(                                     ---                                       )" << endl;
      cout << R"(                                     / | [                                     )" << endl;
      cout << R"(                              !      | |||                                     )" << endl;
      cout << R"(                            _/|     _/|-++'                                    )" << endl;
      cout << R"(                        +  +--|    |--|--|_ |-                                 )" << endl;
      cout << R"(                     { /|__|  |/\__|  |--- |||__/                              )" << endl;
      cout << R"(                    +---------------___[}-_===_.'____                 /\       )" << endl;
      cout << R"(                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/    _  )" << endl;
      cout << R"( __..._____--==/___]_|__|_____________________________[___\==--____,------' .7 )" << endl;
      cout << R"(|                        Welcome to Battleship                         BB-61/  )" << endl;
      cout << R"( \_________________________________________________________________________|   )" << endl;
      cout << endl;
      cout << Color::BG_DEFAULT;
      cout << Color::FG_DEFAULT;



	  InitializeGame();

      StartGame();
    }

    void Program::StartGame()
    {
      //Console::Clear();
      cout << R"(                  __     )" << endl;
      cout << R"(                 /  \    )" << endl;
      cout << R"(           .-.  |    |   )" << endl;
      cout << R"(   *    _.-'  \  \__/    )" << endl;
      cout << R"(    \.-'       \         )" << endl;
      cout << R"(   /          _/         )" << endl;
      cout << R"(  |      _  /""          )" << endl;
      cout << R"(  |     /_\'             )" << endl;
      cout << R"(   \    \_/              )" << endl;
      cout << R"(    """"""""             )" << endl;

      std::list<Position> computerShoots;

      do
      {
        cout << endl;
        cout << R"(Player, it's your turn   )" << endl;

        cout << "-------------------------------------" << endl;
        printEnemySunkShips();
        cout << endl;
        printEnemyLeftoverShips();
        cout << "-------------------------------------" << endl;

		bool PositionValid = false;
		string input;
		Position position;

		cout << R"(Enter coordinates for your shot :   )" << endl;
		

        while(true){
            try {
                getline(cin, input);
                position = ParsePosition(input);
                break;
            }
            catch (...) {
                cout << Color::FG_RED;
                cout << "Invalid entry. Please enter new position: " << endl;
                cout << Color::FG_DEFAULT;
            }
        }
		

        bool isHit = GameController::GameController::CheckIsHit(enemyFleet, position);
        if (isHit)
        {
            // Console::Beep();
            cout << Color::FG_GREEN;
			cout << R"(                \         .  ./         )" << endl;
            cout << R"(              \      .:"";'.:..""   /   )" << endl;
            cout << R"(                  (M^^.^~~:.'"").       )" << endl;
            cout << R"(            -   (/  .    . . \ \)  -    )" << endl;
            cout << R"(               ((| :. ~ ^  :. .|))      )" << endl;
            cout << R"(            -   (\- |  \ /  |  /)  -    )" << endl;
            cout << R"(                 -\  \     /  /-        )" << endl;
            cout << R"(                   \  \   /  /          )" << endl;
            cout << ("Yeah ! Nice hit !") << endl;
            cout << Color::FG_DEFAULT;
            enemyFleetSize--;
		}
		else
		{
            cout << Color::FG_RED;
			cout << ("Miss") << endl;
            cout << Color::FG_DEFAULT;
		}

        do {
            position = GetRandomPosition();
        } while (std::find(computerShoots.begin(), computerShoots.end(), position) != computerShoots.end());

        computerShoots.emplace_back(position);
        isHit = GameController::GameController::CheckIsHit(myFleet, position);
        cout << endl;

        if (isHit)
        {
            //Console::Beep();
            cout << Color::FG_RED;
			cout << R"(                \         .  ./         )" << endl;
            cout << R"(              \      .:"";'.:..""   /   )" << endl;
            cout << R"(                  (M^^.^~~:.'"").       )" << endl;
            cout << R"(            -   (/  .    . . \ \)  -    )" << endl;
            cout << R"(               ((| :. ~ ^  :. .|))      )" << endl;
            cout << R"(            -   (\- |  \ /  |  /)  -    )" << endl;
            cout << R"(                 -\  \     /  /-        )" << endl;
            cout << R"(                   \  \   /  /          )" << endl;

            cout << "(Computer shoot in " << position << " and " << "hit your ship !)" << endl;
            cout << Color::FG_DEFAULT;
            myFleetSize--;
        }
		else
		{
            cout << Color::FG_GREEN;
            cout << "(Computer shoot in " << position << " and missed )   " << endl;
            cout << Color::FG_DEFAULT;
		}
        PrintDoubleSeparator();
      }
      while (enemyFleetSize != 0 && myFleetSize != 0);

      if (enemyFleetSize != 0) {
          cout << Color::FG_RED;
          cout << "You lost" << endl;
          cout << Color::FG_DEFAULT;
#ifdef __linux__
          system("aplay /home/devel/workspace/game/zuhlke-02-battleship-cpp/Battleship.Ascii.Tests/Loser.mid");
#elif _WIN32
          system("start C:\\Users\\andi\\Desktop\\workspace\\zuhlke-02-battleship-cpp\\Battleship.Ascii.Tests\\Loser.mid");
#endif
      }
      else {
          cout <<  Color::FG_GREEN;
          cout << "You are the winner" << endl;
          cout << Color::FG_DEFAULT;
#ifdef __linux__
          system("aplay /home/devel/workspace/game/zuhlke-02-battleship-cpp/Battleship.Ascii.Tests/We_Are_The_Champions.mid");
#elif _WIN32
          system("start C:\\Users\\andi\\Desktop\\workspace\\zuhlke-02-battleship-cpp\\Battleship.Ascii.Tests\\We_Are_The_Champions.mid");
#endif
      }


    }

	Position Program::ParsePosition(string input)
    {
      char cColumn = toupper(input.at(0));

      std::string row = input.substr(1, input.length() - 1);
      int roww = std::atoi(row.c_str());
   

	  int nColumn = (cColumn - 'A');
      Letters lColumn = (Letters)nColumn;


      if (nColumn > BOARD_WIDTH || nColumn < 0 || roww > BOARD_HEIGHT || roww < 0 || input.size() > 3) {
          throw new std::out_of_range("Invalid position");
      }


	  Position outPosition;
	  outPosition.Column = lColumn;
	  outPosition.Row = roww;
	  return outPosition;
    }

    Position Program::GetRandomPosition()
    {
      srand((unsigned int) time(NULL));
      Letters lColumn = (Letters)(rand() % BOARD_WIDTH);
      int nRow = (rand() % BOARD_HEIGHT);

      Position position(lColumn, nRow);
      return position;
    }

    void Program::InitializeGame()
    {
        while (true) {
            try {
                InitializeBoard();
                break;
            }
            catch (...) {
                cout << Color::FG_RED;
                cout << "Invalid entry. Please enter valid size: " << endl;
                cout << Color::FG_DEFAULT;
            }
        }
      

      InitializeMyFleet();

      InitializeEnemyFleet(enemyFleet);
    }

    void Program::InitializeBoard() {
        string input;

        cout << "Please enter board size (max 26 in both directions, min board size is 20 fields) :" << endl;
        cout << "Width :" << endl;
        getline(cin, input);
        BOARD_WIDTH = atoi(input.c_str());

  
        cout << "Height :" << endl;
        getline(cin, input);
        BOARD_HEIGHT = atoi(input.c_str());

        if (BOARD_WIDTH > 26 || BOARD_WIDTH < 0 || BOARD_HEIGHT > 26 || BOARD_HEIGHT < 0 || (BOARD_HEIGHT * BOARD_WIDTH < 20)) {
            throw new std::out_of_range("Invalid position");
        }
        cout << "w " << BOARD_WIDTH << "    h " << BOARD_HEIGHT << endl;
    }

	void Program::InitializeMyFleet()
	{
		myFleet = GameController::GameController::InitializeShips();
		cout << "Please position your fleet (Game board has size from A to Z and 1 to 26) :" << endl;
		for_each(myFleet.begin(), myFleet.end(), [](Ship &ship)
        {
            myFleetSize += ship.Size;
            PrintSeparator();
            cout << "Please enter the positions for the " << ship.Name << " (size: " << ship.Size << ")" << endl;
            for (int i = 1; i <= ship.Size; i++)
            {
              cout << "Enter position " << i << " of " << ship.Size << "\n";
              string input;
              

              Position inputPosition;


              while (true) {
                  try {
                      getline(cin, input);
                      inputPosition = ParsePosition(input);
                      break;
                  }
                  catch (...) {
                      cout << Color::FG_RED;
                      cout << "Invalid entry. Please enter new position: " << endl;
                      cout << Color::FG_DEFAULT;
                  }
              }


              ship.AddPosition(inputPosition);
            }
		});
        PrintDoubleSeparator();
	}

	void Program::InitializeEnemyFleet(list<Ship>& Fleet)
	{
		Fleet = GameController::GameController::InitializeShips();

		for_each(Fleet.begin(), Fleet.end(), [](Ship& ship)
		{
            enemyFleetSize += ship.Size;
			//if (ship.Name == "Aircraft Carrier")
			//{
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 4));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 5));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 6));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 7));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 8));
			//}
			//if (ship.Name == "Battleship")
			//{
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 5));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 6));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 7));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 8));
			//}
			//if (ship.Name == "Submarine")
			//{
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 3));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 3));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 3));
			//}
			//if (ship.Name == "Destroyer")
			//{
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::F, 8));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::G, 8));
			//	ship.Positions.insert(ship.Positions.end(), Position(Letters::H, 8));
			//}
			if (ship.Name == "Patrol Boat")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 6));
			}
		});
        PrintDoubleSeparator();
	}

  	void Program::printEnemySunkShips()
    {
        bool hasSunkenShips = false;
    	
        cout << "Sunken ships: " << endl;
        for_each(enemyFleet.begin(), enemyFleet.end(), [&hasSunkenShips](Ship& ship)
            {
                if (ship.isSunk)
                {
                    hasSunkenShips = true;
                    cout << ship.Name << " coordinates: [ ";
                    for_each(ship.Positions.begin(), ship.Positions.end(), [](Position& position)
                        {
                            cout << position.Row << position.Column << " ";
                        }
                    );
                    cout << "]" << endl;
                }
            }
        );
    	
        if (!hasSunkenShips)
            cout << "You haven't sunk any enemy ship yet"<<endl;
    	
    }

    void Program::printEnemyLeftoverShips()
    {
        bool hasLeftoverShips = false;
    	
        cout << "Leftover ships: " << endl;
        for_each(enemyFleet.begin(), enemyFleet.end(), [&hasLeftoverShips](Ship& ship)
            {
                if (!ship.isSunk)
                {
                    hasLeftoverShips = true;
                    cout << ship.Name << " size: " << ship.Size << endl;
                }
            }
        );

        if (!hasLeftoverShips)
            cout << "You have sunk all enemy ships!" << endl;
    }

      void Program::PrintSeparator() {
        cout << Color::FG_YELLOW;
        cout << DASHES;
        cout << Color::FG_DEFAULT;
        cout << Color::BG_DEFAULT;
    }

    void Program::PrintDoubleSeparator() {
        cout << Color::FG_YELLOW;
        cout << DOUBLE_DASHES;
        cout << Color::FG_DEFAULT;
        cout << Color::BG_DEFAULT;
    }
  }
}
