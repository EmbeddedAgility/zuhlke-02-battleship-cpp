#pragma once

#include <list>
#include <ostream>
#include "../Battleship.GameController.Lib/Ship.h"

using namespace std;
using namespace Battleship::GameController::Contracts;

namespace Color {
enum Code {
    FG_DEFAULT = 39,
    FG_BLACK = 30,
    FG_RED = 31,
    FG_GREEN = 32,
    FG_YELLOW = 33,
    FG_BLUE = 34,
    FG_MAGENTA = 35,
    FG_CYAN = 36,
    FG_LIGHT_GRAY = 37,
    FG_DARK_GRAY = 90,
    FG_LIGHT_RED = 91,
    FG_LIGHT_GREEN = 92,
    FG_LIGHT_YELLOW = 93,
    FG_LIGHT_BLUE = 94,
    FG_LIGHT_MAGENTA = 95,
    FG_LIGHT_CYAN = 96,
    FG_WHITE = 97,
    BG_RED = 41,
    BG_GREEN = 42,
    BG_BLUE = 44,
    BG_DEFAULT = 49
};

std::ostream& operator<<(std::ostream& os, Code code);

}

namespace Battleship
{
  namespace Ascii
  {
	class Program
	{
	private:
	  static list<Ship> myFleet;
	  static list<Ship> enemyFleet;
	  static int myFleetSize;
	  static int enemyFleetSize;

	public:
	  Program();
	  ~Program();

	public:
	  static void Main();
	  static void StartGame();

	private:
	  static void InitializeGame();

      static void InitializeBoard();
	  static void InitializeMyFleet();
      static void InitializeEnemyFleet(list<Ship> &Fleet);
      static void PrintSeparator();
      static void PrintDoubleSeparator();

	public:
	  static Position ParsePosition(string input);
	  static Position GetRandomPosition();
	  static void printEnemySunkShips();
	  static void printEnemyLeftoverShips();
	};
  }
}

